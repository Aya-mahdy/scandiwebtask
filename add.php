<?php
include 'core/product_class.php';
include 'core/db_connection.php';
include 'core/validation.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css"/>

    <title>Product Add</title>

  </head>
  <body>

  <div class="container">

                        <?php
                            if(isset($_POST['save'])) {
				// decomposing and preparation
				foreach ($_POST as $key => $value) {
				$$key = prepareInput($value);
				}

				// validation
                                // sku: required,unique,max:255
                                if(! isRequired($sku)) {
                                    $errors['sku'] = "required";
                                }elseif(! isUnique($sku)){
                                    $errors['sku'] = "sku already exists";
                                }elseif(! isNotMoreThan($sku, 255)) {
                                    $errors['sku'] = "sku is too long";
                                

                                }
                                // name: required,strig,max:255
                                    if(! isRequired($name)) {
                                        $errors['name'] = "required";
                                    }elseif(isNumber($name)){
                                        $errors['name'] = "name must be letters only";
                                    }elseif(! isNotMoreThan($name, 255)) {
                                        $errors['name'] = "name is too long";
                                    }

                                // price: required,integer,max:255
                                if(! isRequired($price)) {
                                    $errors['price'] = "required";
                                }elseif(! isNumber($price)){
                                    $errors['price'] = "price must be numbers only";
                                }elseif(! isNotMoreThan($price, 255)) {
                                    $errors['price'] = "price is too long";
                                }

                                //producttype: required
                                if($productType != 'dvd' 
                                && $productType != 'book' && $productType != 'furniture')
                                {
                                    $errors['productType'] = "required";
                                }

                                // size: required,integer,max:255
                                if(! isRequired($size)) {
                                    $type_errors['size'] = "required";
                                }elseif(! isNumber($size)){
                                    $type_errors['size'] = "size must be numbers only";
                                }elseif(! isNotMoreThan($size, 255)) {
                                    $type_errors['size'] = "size is too long";
                                }

                            	// weight: required,integer,max:255
                                if(! isRequired($weight)) {
                                    $type_errors['weight'] = "required";
                                }elseif(! isNumber($weight)){
                                    $type_errors['weight'] = "weight must be numbers only";
                                }elseif(! isNotMoreThan($weight, 255)) {
                                    $type_errors['weight'] = "weight is too long";
                                }
    
                                 // height: required,integer,max:255
                                 if(! isRequired($height)) {
                                    $type_errors['height'] = "required";
                                }elseif(! isNumber($height)){
                                    $type_errors['height'] = "height must be numbers only";
                                }elseif(! isNotMoreThan($height, 255)) {
                                    $type_errors['height'] = "height is too long";
                                }

                                // width: required,integer,max:255
                                if(! isRequired($width)) {
                                    $type_errors['width'] = "required";
                                }elseif(! isNumber($width)){
                                    $type_errors['width'] = "width must be numbers only";
                                }elseif(! isNotMoreThan($width, 255)) {
                                    $type_errors['width'] = "width is too long";
                                }

                             // length: required,integer,max:255
                                if(! isRequired($length)) {
                                    $type_errors['length'] = "required";
                                }elseif(! isNumber($length)){
                                    $type_errors['length'] = "length must be numbers only";
                                }elseif(! isNotMoreThan($length, 255)) {
                                    $type_errors['length'] = "length is too long";
                                }

                           


                            //if valid data & dvd type
                            if(empty($errors) && $_POST['size'] != null)
                            {
                                $data =[
                                    'sku' => $sku,
                                    'name' => $name,
                                    'price' => $price,
                                    'size' => $size
                                    
                                ];

                                $dvd = new Dvd();
                                $dvd->saveRecord($data['sku'],$data['name'],$data['price'],$data['size']);
                            }elseif(empty($errors) && $_POST['weight'] != null)
                            {
                                $data =[
                                    'sku' => $_POST['sku'],
                                    'name' => $_POST['name'],
                                    'price' => $_POST['price'],
                                    'weight' => $_POST['weight']
                                    
                                    
                                ];

                                $book = new Book();
                                $book->saveRecord($data['sku'],$data['name'],$data['price'],$data['weight']);
                            }elseif(empty($errors) && $_POST['height'] != null)
                            {
                                $data =[
                                    'sku' => $_POST['sku'],
                                    'name' => $_POST['name'],
                                    'price' => $_POST['price'],
                                    'height' => $_POST['height'],
                                    'width' => $_POST['width'],
                                    'length' => $_POST['length']

                                    
                                    
                                ];

                                $furniture = new Furniture();
                                $furniture->saveRecord($data['sku'],$data['name'],$data['price'],$data['height'],$data['width'],$data['length']);
                            }
                            }
                           
                        ?>
            <form id="product_form" method="POST" action="">
                    <div class="row d-flex flex-md-nowrap">
                                    <div class="col-md-10 mt-md-5">
                                        <h1>Product Add</h1>
                                    </div>

                                    <div class="col-md-4 mt-md-5">
                                            
                                                <input type="submit" class="btn btn-light" name="save" value="Save">
                                                <a href="index.php" class="btn btn-light">Cancel</a>
                                                
                                            
                                            
                                            <!--<button id="delete-product-btn" type="button" class="btn btn-light mt-md-5">MASS DELETE</button>-->
                                    </div>
                    </div>
                    <hr>
                    <div class="row d-flex flex-md-nowrap">
                        <div class="col-md-4">
                            <div class="form-group row m-3">
                                <label class="col-sm-2 col-form-label">SKU</label>
                                <div class="col-sm-10">
                                <input id="sku" name="sku" type="text" 
                                value="<?php if(isset($_POST['sku'])) echo $_POST['sku']; ?>"
                                 class="form-control mb-2">
                                 <?php getError('sku');?>
                                </div>

                                
                            </div>

                            <div class="form-group row m-3">
                                <label class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                <input id="name" name="name" type="text" value="<?php if(isset($_POST['name'])) echo $_POST['name']; ?>" class="form-control">
                                <?php getError('name');?>
                                </div>
                            </div>

                            <div class="form-group row m-3">
                                <label class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                <input id="price" name="price" type="number" value="<?php if(isset($_POST['price'])) echo $_POST['price']; ?>" class="form-control">
                                <?php getError('price');?>
                                </div>
                            
                            </div>

                            <label for="productType">Type Switcher:</label>
                            <select name="productType" id="productType">
                                    <option value="" selected>Type</option>
                                    <option value="dvd">DVD</option>
                                    <option value="book">Book</option>
                            
                                    <option value="furniture">Furniture</option>
                                   

                            </select>
                            <?php getError('productType');?>

                            <div id="dvd" class="form-group row m-3" style="display: none;">
                                <label class="col-sm-2 col-form-label">Size (MB)</label>
                                <div class="col-sm-10">
                                <input id="size" name="size" type="number"  class="form-control">
                                <?php getError('size');?>
                                <span>*Please provide size like 40MB</span>
                                </div>
                            </div>

                            
                            <div id="book" class="form-group row m-3" style="display: none;">
                                <label class="col-sm-2 col-form-label">Weight (KG)</label>
                                <div class="col-sm-10">
                                <input id="weight" name="weight" type="number" class="form-control">
                                <?php getError('weight');?>
                                <span>*Please provide weight like 50KG</span>
                                </div>

                            </div>
                            
                            
                            <div id="furniture" class="form-group row m-3" style="display: none;">
                                <label class="col-sm-2 col-form-label">Height (CM)</label>
                                <div class="col-sm-10">
                                <input id="height" name="height" type="number" class="form-control">
                                <?php getError('height');?>
                                </div>

                                <label class="col-sm-2 col-form-label">Width (CM)</label>
                                <div class="col-sm-10">
                                <input id="width" name="width" type="number" class="form-control">
                                <?php getError('width');?>
                                
                                </div>

                                <label class="col-sm-2 col-form-label">Length (CM)</label>
                                <div class="col-sm-10">
                                <input id="length" name="length" type="number" class="form-control">
                                <?php getError('length');?>
                                </div>
                                <span>*Please provide dimensions like (40x50x60)</span>
                                
                            </div>

                        
                           

                            
                            


                        </div>

                    </div>      

                    

            
            </form>
    <div>



    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">


        $(document).ready(function(){
            $('#productType').on('change', function() {
            if ( this.value == 'dvd')
            //.....................^.......
            {
                $("#dvd").show();
            }
            else
            {
                $("#dvd").hide();
            }
            });

            $('#productType').on('change', function() {
            if ( this.value == 'book')
            //.....................^.......
            {
                $("#book").show();
            }
            else
            {
                $("#book").hide();
            }
            });

            $('#productType').on('change', function() {
            if ( this.value == 'furniture')
            //.....................^.......
            {
                $("#furniture").show();
            }
            else
            {
                $("#furniture").hide();
            }
            });
        });

  </script>
  </body>


</html>

