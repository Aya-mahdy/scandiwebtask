<?php
include 'core/db_connection.php';
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="assets/style.css"/>

    <title>Products list</title>
  </head>
  <body>
   
    
    <div class="container">
                        
            <form method="POST" action="core/delete_product.php">
                <div class="row d-flex flex-md-nowrap">
                            <div class="col-md-10 mt-md-5">
                                <h1>Product list</h1>
                            </div>
                            

                            <div class="col-md-4 mt-md-5">
                                    
                                    
                                        <a href="add.php" class="btn btn-light">ADD</a>
                                        <input id="delete-product-btn" type="submit" class="btn btn-light" name="delete_records" value="MASS DELETE">
                                    
                                    
                                    <!--<button id="delete-product-btn" type="button" class="btn btn-light mt-md-5">MASS DELETE</button>-->
                            </div>
                    
                        
                        
                        
                </div>
                <hr>
                
                <div class="row">
                                <?php 
                                    $results = "SELECT * FROM products";
                                    
                                    $results = $connect->query($results);
                                    if ($results->num_rows > 0)
                                    {
                                        while($row=mysqli_fetch_assoc($results))
                                        {
                                            ?>
                                            <div class="col-md-3 mx-3 my-2">
                                                
                                                        
                                                        <input class="delete-checkbox" type="checkbox" name="no[]" value="<?php echo $row['id'];?>">
                                                            
                                                        <span><?php echo $row['sku'];?> </span></br>
                                                        <span><?php echo $row['name'];?></span></br>
                                                        <span><?php echo $row['price'];?></span> $</br>
                                                        <?php
                                                        if($row['size'] != null ){
                                                            echo "<span>Size: ". $row['size'] .' MB'."</span>";
                                                        }elseif($row['weight'] != null)
                                                        {
                                                            echo "<span>Weight: ". $row['weight'] .' KG'."</span>";
                                                        }elseif($row['height'] != null)
                                                        {
                                                            echo "<span>Dimension: ". $row['height'] .'x'.$row['width'] .'x'.$row['length']."</span>";
                                                        }
                                                        ?>

                                                
                                                        
                                                
                                            
                                            
                                            </div>
                                            <?php
                                            
                                        }
                                    }
                                ?>
                        
                        
                            
                </div>
            </form>
            
    
    
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  </body>
</html>