<?php
include 'db_connection.php';

$errors = [];
$type_errors = [];



function prepareInput(string $input): string  
{
  return trim(htmlspecialchars($input));
}

function isRequired($input): bool
{
  return (! empty($input));
}


function isNotMoreThan($input, int $maxLength): bool
{
  $length = strlen($input);

  return ($length <= $maxLength);
}

function isNumber($input): bool
{
    return(is_numeric($input));
}


function isUnique($input) : bool 
{
  global $connect;
  $result = "SELECT sku FROM products Where sku='$input' ";      
  $result = $connect->query($result);
  return (!$result->num_rows);
}

function getError(string $key) 
{
  global $errors;
  global $type_errors;

  if(isset($errors[$key])) {
    echo "<span style='color:red;'> * " . $errors[$key] . "</span>";
  }elseif(isset($type_errors[$key]))
  {
    echo "<span style='color:red;'> * " . $type_errors[$key] . "</span>";
  }
    
}

